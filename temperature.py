def convert_cel_to_far(C):
    F=C*9/5+32
    return F
def convert_far_to_cel(F):
    C=(F-32)*(5/9)
    return C

temp1 = (input("Enter a temperature in degrees F: "))
tempC = convert_far_to_cel(float(temp1))
print(f"{temp1} degrees F = {tempC:.2f} degrees C")

temp2 = (input("Enter a temperature in degrees C: "))
tempF = convert_cel_to_far(float(temp2))
print(f"{temp2} degrees C = {tempF:.2f} degrees F")
